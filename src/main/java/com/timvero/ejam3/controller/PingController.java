package com.timvero.ejam3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.timvero.ejam3.data.user.UserRepository;

@RestController
@PreAuthorize("permitAll()")
public class PingController {
	
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping("ping")
	public String ping(@RequestParam(required = false, defaultValue = "pong") String value) {
		return value;
	}

}
