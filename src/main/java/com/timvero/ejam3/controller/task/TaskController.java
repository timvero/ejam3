package com.timvero.ejam3.controller.task;

import com.timvero.ejam3.data.task.Task;
import com.timvero.ejam3.data.task.TaskDifficulty;
import com.timvero.ejam3.data.task.TaskForm;
import com.timvero.ejam3.data.task.TaskService;
import com.timvero.ejam3.security.SecurityUser;

import javassist.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("task")
public class TaskController {
    
	@Autowired
	TaskService service;

	@RequestMapping(value = "/{taskId}", method=RequestMethod.GET)
	public Task getTask(@PathVariable UUID taskId) {
		return service.get(taskId);
	}

	@RequestMapping(value = "/author/{authorId}", method=RequestMethod.GET)
	public List<Task> getAuthorTasks(@PathVariable UUID authorId) {
		return service.getTaskByAuthor(authorId);
	}

	@RequestMapping(value = "/all", method=RequestMethod.GET)
	public List<Task> getTasks(@RequestParam(required = false) List<String> tags, @RequestParam(required = false)TaskDifficulty difficulty) {
		return service.getAll(tags, difficulty);
	}
	
    @RequestMapping(value = "/author/all", method = RequestMethod.GET)
    public List<Task> listAuthorTasks(SecurityUser user, @RequestParam(required = false) List<String> tags, @RequestParam(required = false)TaskDifficulty difficulty) {
    	return service.getAuthorTasks(user.getId(), tags, difficulty);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
	public Task saveOrUpdate(@RequestBody TaskForm form, SecurityUser user){
		return service.addNewTask(form, user.getId());
	}
    
    @RequestMapping(value = "/upvote/{taskId}", method = RequestMethod.POST)
    public void saveOrUpdate(@PathVariable UUID taskId, SecurityUser user) throws NotFoundException{
    	service.upvote(taskId, user.getId());
    }
    
}
