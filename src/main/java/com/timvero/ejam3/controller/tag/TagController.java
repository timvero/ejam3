package com.timvero.ejam3.controller.tag;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("tag")
public class TagController {

    private List<String> tags;
    {
        tags = new ArrayList<>();
        tags.add("Начальная школа");
        tags.add("Средняя школа");
        tags.add("Математика");
        tags.add("Физика");
        tags.add("Химия");
        tags.add("Эволюция человека");
        tags.add("Астрономия");
        tags.add("История");
        tags.add("Литература");
        tags.add("Биология");
        tags.add("География");
        tags.add("Информатика");
        tags.add("Черчение");
        tags.add("Философия");
        tags.add("Правоведение");
        tags.add("Экология");
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<String> allTags(){
        return tags;
    }
}
