package com.timvero.ejam3.data.task;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends CrudRepository<Task, UUID> {
	
	List<Task> findByAuthorId(UUID id);

	List<Task> findAll();
	
	List<Task> findAllByOrderByCreatedAtDesc();
	
	List<Task> findAllByTopicInOrderByCreatedAtDesc(List<String> topic);
	
	List<Task> findAllByOrderByRatingDesc();
}
