package com.timvero.ejam3.data.task;

public class Topic {

    private String tag;
    private Task task;

    public Topic() {
    }

    public Topic(String tag, Task task) {
        this.tag = tag;
        this.task = task;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}
