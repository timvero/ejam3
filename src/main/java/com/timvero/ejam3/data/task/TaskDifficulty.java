package com.timvero.ejam3.data.task;

public enum TaskDifficulty {
	ELEMENTARY, INTERMEDIATE, ADVANCED, PROFICIENT, UNBELIEVABLE
}
