package com.timvero.ejam3.data.task;

import com.timvero.ejam3.data.rating.RatingEntity;
import com.timvero.ejam3.data.rating.RatingRepository;
import com.timvero.ejam3.data.rating.RatingValue;
import com.timvero.ejam3.data.user.User;
import com.timvero.ejam3.data.user.UserRepository;

import javassist.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@CacheConfig(cacheNames={"tasks"})
public class TaskService {
	
	@Autowired
	private TaskRepository repository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RatingRepository ratingRepository;
	
	public Task get(UUID id) {
		return repository.findById(id).orElse(null);
	}

	public List<Task> getTaskByAuthor(UUID id) {
		return repository.findByAuthorId(id);
	}

	@Cacheable
	public List<Task> getAll(List<String> tags, TaskDifficulty difficulty) {
		List<Task> tasks;
		if (tags == null || tags.isEmpty()) {
			tasks = repository.findAllByOrderByCreatedAtDesc();
		} else {
			tasks = repository.findAllByTopicInOrderByCreatedAtDesc(tags).stream()
					.filter(t -> t.getTopic().containsAll(tags))
					.collect(Collectors.toList());
		}
		if(difficulty != null){
			return tasks.stream().filter(t -> t.getDifficulty() == difficulty).collect(Collectors.toList());
		}
		return tasks;
	}
	
	@CacheEvict(allEntries = true)
	public Task addNewTask(TaskForm form, UUID userId) {
	    User user = userRepository.findById(userId).orElse(null);
		Task task = new Task(form.getTitle(), form.getDescription(), form.getTags(), form.getDifficulty(), TaskStatus.NEW, user);
		return repository.save(task);
	}

	public List<Task> getAuthorTasks(UUID authorId, List<String> tags, TaskDifficulty difficulty){
		List<Task> tasks;
		if (tags == null || tags.isEmpty()) {
			tasks = repository.findByAuthorId(authorId);
		} else {
			tasks = repository.findByAuthorId(authorId).stream()
					.filter(t -> t.getTopic().containsAll(tags))
					.collect(Collectors.toList());
		}
		if(difficulty != null){
			return tasks.stream().filter(t -> t.getDifficulty() == difficulty).collect(Collectors.toList());
		}
		return tasks;
	}
	
	@CacheEvict(allEntries = true)
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void upvote(UUID taskId, UUID userId) throws NotFoundException {
		RatingEntity rating = ratingRepository.findByTaskIdAndUserId(taskId, userId).orElse(null);
		if (rating == null) {
			Task task = repository.findById(taskId).orElseThrow(() -> new NotFoundException("task with id:" + taskId + " not found"));
			User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("task with id:" + taskId + " not found"));
			if (task.getStatus() == TaskStatus.NEW) {
				task.setStatus(TaskStatus.APPROVED);
			}
			task.setRating(task.getRating().add(BigDecimal.ONE));
			ratingRepository.save(new RatingEntity(RatingValue.UPVOTE, user, task));
		}
	}
	
}
