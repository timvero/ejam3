package com.timvero.ejam3.data.task;

import javax.persistence.*;

import com.timvero.ejam3.data.BaseEntity;
import com.timvero.ejam3.data.user.User;

import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "task")
public class Task extends BaseEntity {
	
	private String title;
	private String description;
	@ElementCollection
	private List<String> topic;
	private TaskDifficulty difficulty;
	private TaskStatus status;
	private BigDecimal rating;
	
    @ManyToOne(fetch = FetchType.EAGER)
    private User author;
	
	public Task() {
		super();
	}

	public Task(String title, String description, List<String> topic, TaskDifficulty difficulty, TaskStatus status, User author) {
		super();
		this.title = title;
		this.description = description;
		this.topic = topic;
		this.difficulty = difficulty;
		this.status = status;
		this.author = author;
		this.rating = BigDecimal.ZERO;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getTopic() {
		return topic;
	}

	public void setTopic(List<String> topic) {
		this.topic = topic;
	}

	public TaskStatus getStatus() {
		return status;
	}

	public void setStatus(TaskStatus status) {
		this.status = status;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}
	public BigDecimal getRating() {
		return rating;
	}
	public void setRating(BigDecimal rating) {
		this.rating = rating;
	}

	public TaskDifficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(TaskDifficulty difficulty) {
		this.difficulty = difficulty;
	}
}
