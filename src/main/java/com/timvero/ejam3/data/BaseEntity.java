package com.timvero.ejam3.data;

import java.time.Instant;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import org.hibernate.annotations.GenericGenerator;

@MappedSuperclass
@GenericGenerator(name = "uuid", strategy = "uuid2")
public class BaseEntity {
	@Id
    @GeneratedValue(generator = "uuid")
    private UUID id;
	
    @Column(name = "created_at", nullable = false, updatable = false)
    private Instant createdAt = Instant.now();
	
	public UUID getId() {
		return id;
	}
	public Instant getCreatedAt() {
		return createdAt;
	}
}