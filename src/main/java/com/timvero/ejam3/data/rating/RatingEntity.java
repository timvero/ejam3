package com.timvero.ejam3.data.rating;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.timvero.ejam3.data.BaseEntity;
import com.timvero.ejam3.data.task.Task;
import com.timvero.ejam3.data.user.User;

@Entity
@Table(name = "rating")
public class RatingEntity extends BaseEntity{
	
	private RatingValue value;
	
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
	private User user;
    
    @ManyToOne(fetch = FetchType.EAGER)
	private Task task;
	
	public RatingEntity() {
		super();
	}
	
	public RatingEntity(RatingValue value, User user, Task task) {
		super();
		this.value = value;
		this.user = user;
		this.task = task;
	}
	
	public RatingValue getValue() {
		return value;
	}
	public void setValue(RatingValue value) {
		this.value = value;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Task getTask() {
		return task;
	}
	public void setTask(Task task) {
		this.task = task;
	}
}
