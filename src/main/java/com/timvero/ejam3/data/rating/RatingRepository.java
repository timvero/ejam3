package com.timvero.ejam3.data.rating;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingRepository extends CrudRepository<RatingEntity, UUID> {
	
	Optional<RatingEntity> findByTaskIdAndUserId(UUID taskId, UUID userId);

}
