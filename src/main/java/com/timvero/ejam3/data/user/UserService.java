package com.timvero.ejam3.data.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder portalPasswordEncoder;

    public UserForm registrationUser(UserForm userForm){
    	if (userRepository.findByUsername(userForm.getUsername()) != null) throw new RuntimeException();
        User user = new User();
        user.setName(userForm.getName());
        user.setUsername(userForm.getUsername());
        user.setPassword(portalPasswordEncoder.encode(userForm.getPassword()));
        user.setDescription(userForm.getDescription());
        if (userForm.getInviter() != null) {
        	User inviter = userRepository.findById(userForm.getInviter()).orElse(null);
        	user.setInviter(inviter);
        }
        user = userRepository.save(user);
        PreAuthenticatedAuthenticationToken token = new PreAuthenticatedAuthenticationToken(user, user, new ArrayList<>());
        SecurityContextHolder.getContext().setAuthentication(token);
        userForm.setPassword(null);
        return userForm;
    }
}
