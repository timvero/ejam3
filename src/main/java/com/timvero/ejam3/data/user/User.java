package com.timvero.ejam3.data.user;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "user_account")
@GenericGenerator(name = "uuid", strategy = "uuid2")
public class User {
	@Id
    @GeneratedValue(generator = "uuid")
    private UUID id;

	private String username;
	
	String name;

	String password;

	String description;
	
	@ManyToOne(fetch = FetchType.LAZY)
    private User inviter;

	public UUID getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public User getInviter() {
		return inviter;
	}
    
    public void setInviter(User inviter) {
		this.inviter = inviter;
	}
}
