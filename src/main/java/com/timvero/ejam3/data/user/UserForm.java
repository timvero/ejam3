package com.timvero.ejam3.data.user;

import java.util.UUID;

public class UserForm {

    private String name;

    private String password;

    private String description;

    private String username;
    
    private UUID inviter;

    public UserForm() {
    }

    public UserForm(String name, String username, String description) {
        this.name = name;
        this.username = username;
        this.description = description;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public UUID getInviter() {
		return inviter;
	}
    
    public void setInviter(UUID inviter) {
		this.inviter = inviter;
	}
}
