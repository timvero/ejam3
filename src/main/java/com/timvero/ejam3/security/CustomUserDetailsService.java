package com.timvero.ejam3.security;

import com.timvero.ejam3.data.user.User;
import com.timvero.ejam3.data.user.UserRepository;
import com.timvero.ejam3.security.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByUsername(username);

        if (user == null) throw new UsernameNotFoundException("User not found");

       return new SecurityUser(user.getId(), user.getUsername(), "qwerty", new ArrayList<>());
    }
}
