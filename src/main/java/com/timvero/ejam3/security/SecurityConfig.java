package com.timvero.ejam3.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.Assert;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, proxyTargetClass = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomUserDetailsService userDetailService;

    private static final MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http.headers().frameOptions().disable().httpStrictTransportSecurity().disable()
        	.and()
        	.antMatcher("/**").csrf().disable()
        	.authorizeRequests()
			.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
        	.antMatchers("/ping").permitAll()
			.antMatchers("/registration").permitAll()
    	.anyRequest().authenticated().and().formLogin().permitAll(false)
        .loginProcessingUrl("/login")
	        .failureHandler((request, response, exception) -> {
	            response.sendError(HttpStatus.UNAUTHORIZED.value());
	        })
	        .successHandler((request, response, authentication) -> {
	            MediaType jsonMimeType = MediaType.APPLICATION_JSON;
	            SecurityUser user = (SecurityUser) authentication.getPrincipal();
	            Assert.state(jsonConverter.canWrite(user.getClass(), jsonMimeType), "Object of class " + user.getClass() + " cannot be mapped to " + jsonMimeType);
	            jsonConverter.write(user, jsonMimeType, new ServletServerHttpResponse(response));
	        })
        .and().exceptionHandling()
	        .authenticationEntryPoint((request, response, authException) -> {
	            response.sendError(HttpStatus.UNAUTHORIZED.value());
	        })
        .and().logout().logoutUrl("/logout")
	        .logoutSuccessHandler((request, response, authentication) -> {
	            response.setStatus(HttpStatus.OK.value());
	        })
        .and().exceptionHandling()
	        .accessDeniedHandler((request, response, accessDeniedException) -> {
	            response.sendError(HttpStatus.FORBIDDEN.value());
	        });
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }
}