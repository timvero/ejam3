package com.timvero.ejam3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@EnableCaching
@PropertySources({
    @PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true),
    @PropertySource(value = "file:${catalina.base}/conf/application.properties", ignoreResourceNotFound = true)})
public class Ejam3Application {

	public static void main(String[] args) {
		SpringApplication.run(Ejam3Application.class, args);
	}

}
